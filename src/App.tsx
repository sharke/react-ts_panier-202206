import { useState } from "react";
import "./assets/scss/App.scss";
import { Route, Routes } from "react-router-dom";
import { Container } from "react-bootstrap";
import Home from "./pages/Home";
import Store from "./pages/Store";
import About from "./pages/About";
import MainNav from "./components/MainNav";

function App() {
  // const [count, setCount] = useState(0);
  return (
    <Container>
      <MainNav></MainNav>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/store" element={<Store />}></Route>
        <Route path="/about" element={<About />}></Route>
      </Routes>
    </Container>
  );
}

export default App;
