import { Navbar, Container, Nav, NavDropdown } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { AiOutlineShoppingCart } from "react-icons/ai";
// AiOutlineShoppingCart

function MainNav() {
  return (
    <Navbar bg="light" expand="md" className="shadow-sm">
      <Container>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link to="/" as={NavLink}>
              Home
            </Nav.Link>
            <Nav.Link to="/store" as={NavLink}>
              store
            </Nav.Link>
            <Nav.Link to="/about" as={NavLink}>
              about
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <button
          className="btn btn-outline-primary rounded-circle"
          style={{ width: "3rem", height: "3rem" }}h
        >
          <AiOutlineShoppingCart />
          <div
            className="rounded-circle"
            style={{ backgroundColor: "red", color: "white", fontWeight: 500 }}
          >
            <span>3</span>
          </div>
        </button>
      </Container>
    </Navbar>
  );
}

export default MainNav;
